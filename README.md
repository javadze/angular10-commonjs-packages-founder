# angular10-commonJs-packages-founder
find angular commonjs packages from "Fix CommonJS or AMD dependencies can cause optimization bailouts" warnnings

## usage
1. serve your app and copy warrnings
2. open this app with vs code --> you must "live server" extention
3. paste warnings to "you'r warrnings" field and click find btn
4. copy "packges" field
5. go to your angular.json file and do this:

```
"build": {
  "builder": "@angular-devkit/build-angular:browser",
  "options": {
     "allowedCommonJsDependencies": [
        //paste them here
     ]
     ...
   }
   ...
},
```


## why are you may want this app
if you have this warnning after updating to angular 10 :
"Fix CommonJS or AMD dependencies can cause optimization bailouts"
than this app will help you to list them instead of copying each of them